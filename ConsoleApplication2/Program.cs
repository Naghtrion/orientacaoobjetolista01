﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcao = 0;
            while (opcao != 6)
            {
                Console.WriteLine("Lista de exercicios 01");
                Console.WriteLine("1 - Exercicio 1");
                Console.WriteLine("2 - Exercicio 2");
                Console.WriteLine("3 - Exercicio 3");
                Console.WriteLine("4 - Exercicio 4");
                Console.WriteLine("5 - Exercicio 5");
                Console.WriteLine("6 - Sair");
                Console.WriteLine();
                Console.Write("Escolha uma opção: ");
                try
                {
                    opcao = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    opcao = 0;
                }

                Console.Clear();

                if (opcao == 1) // Exercicio 01
                {

                }
                else if (opcao == 2) // Exercicio 02
                {
                    Console.WriteLine("Exercicio 02");
                    Console.WriteLine(gerarNomes());
                }
                else if (opcao == 3) // Exercico 03
                {
                    Console.WriteLine("Exercicio 03");
                    Console.WriteLine("Digite um frase com mais de 45 caracteres:");
                    String nagh = Console.ReadLine();
                    String[] palavras = nagh.Split(' ');

                    if (palavras.Length < 45)
                        Console.WriteLine("Escreva mais de 45 palavras! Total escrito: " + palavras.Length);
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine(formatarFrase(palavras));
                    }
                }
                else if (opcao == 4) // Exercicio 04
                {
                    Console.WriteLine("Exercicio 04");
                    int[] vet = geraVetor(60, 10, 50);

                    int tamanho = vet.Length;
                    for (int i = 0; i < tamanho; i += 1)
                        Console.Write(vet[i] + "   ");
                }
                else if (opcao == 5) // Exercicio 05
                {
                    Console.WriteLine("Exercicio 05");
                    string arquivo, palavra;

                    // Digite isso para testar: teste.txt
                    Console.Write("Informe o diretorio do arquivo: ");
                    arquivo = Console.ReadLine();

                    while (!File.Exists(arquivo))
                    {
                        Console.WriteLine("Este arquivo não existe!");
                        Console.Write("Informe o diretorio do arquivo: ");
                        arquivo = Console.ReadLine();
                    }

                    Console.Write("Informe a palavra a ser procurada: ");
                    palavra = Console.ReadLine();

                    List<int> list = pesquisaPalavras(arquivo, palavra);

                    Console.WriteLine("A palavra " + " foi encontrada nas linhas:");
                    foreach (int prime in list)
                        Console.WriteLine("- #" + prime);
                }

                if (opcao != 6)
                {
                    Console.WriteLine();
                    Console.WriteLine("Pressione qualquer tecla para continuar...");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }


        // Ex.02
        static string gerarNomes()
        {
            String letras = "bcdfghjklmnpqrstvwxyz";
            String vogais = "aeiou";
            String result = "";
            String nome = "";

            Random r = new Random();
            for (int i = 0; i < 5; i++)
            {
                nome = "";
                switch (r.Next(3))
                {
                    case 1:
                        nome = "" + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + "";
                        break;
                    case 2:
                        nome = "" + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + "";
                        break;
                    default:
                        nome = "" + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + letras[r.Next(letras.Length)] + vogais[r.Next(vogais.Length)] + "";
                        break;
                }

                result += nome + " ";
            }
            return result;
        }


        // Ex.03
        static string formatarFrase(String[] palavras)
        {
            String result = "";
            int lorem = 41;

            for (int i = 0; i < palavras.Length; i += 1)
            {
                if ((result + " " + palavras[i]).Length < lorem)
                {
                    result += " " + palavras[i];
                }
                else
                {
                    i -= 1;
                    result += "\n";
                    lorem = result.Length + 41;
                }
            }

            return result;
        }


        //Ex.04
        static int[] geraVetor(int tamanho, int minimo, int maximo)
        {
            int[] vet = new int[tamanho];

            Random r = new Random();

            for (int i = 0; i < tamanho; i += 1)
                vet[i] = r.Next(minimo, maximo);

            return vet;
        }


        //Ex.05
        static List<int> pesquisaPalavras(string arquivo, string palavra)
        {
            List<int> list = new List<int>();
            StreamReader sr = new StreamReader(arquivo);
            for (int i = 0; !sr.EndOfStream; i += 1)
            {
                string linha = sr.ReadLine();
                if (linha.IndexOf(palavra) >= 0)
                {
                    list.Add((i + 1));
                }
            }
            sr.Close();

            return list;
        }
    }
}
